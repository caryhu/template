var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({
                    __proto__: []
                }
                instanceof Array && function (d, b) {
                    d.__proto__ = b;
                }) ||
            function (d, b) {
                for (var p in b)
                    if (b.hasOwnProperty(p)) d[p] = b[p];
            };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);

        function __() {
            this.constructor = d;
        }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var typedoc;
(function (typedoc) {
    typedoc.$html = $('html');
    var services = [];
    var components = [];
    typedoc.$document = $(document);
    typedoc.$window = $(window);
    typedoc.$body = $('body');

    function registerService(constructor, name, priority) {
        if (priority === void 0) {
            priority = 0;
        }
        services.push({
            constructor: constructor,
            name: name,
            priority: priority,
            instance: null
        });
        services.sort(function (a, b) {
            return a.priority - b.priority;
        });
    }
    typedoc.registerService = registerService;
    var _isIE9 = navigator.appVersion.indexOf('MSIE 9') > -1;

    function isIE9() {
        return _isIE9;
    }

    function addClass(e, className) {
        if (e && className) {
            if (e instanceof HTMLElement && e.classList && !isIE9()) {
                if (className.indexOf(' ') < 0) {
                    e.classList.add(className);
                } else {
                    var classes = className.split(' ');
                    for (var i = 0; i < classes.length; i++) {
                        e.classList.add(classes[i]);
                    }
                }
                return;
            }
            if (e.setAttribute) {
                var classes = className.split(' ');
                for (var i = 0; i < classes.length; i++) {
                    var cls = classes[i];
                    if (!hasClass(e, cls)) {
                        var cn = e.getAttribute('class');
                        e.setAttribute('class', cn ? cn + ' ' + cls : cls);
                    }
                }
            }
        }
    }
    typedoc.addClass = addClass;

    function removeClass(e, className) {
        if (e && className) {
            if (e instanceof HTMLElement && e.classList && !isIE9()) {
                if (className.indexOf(' ') < 0) {
                    e.classList.remove(className);
                } else {
                    var classes = className.split(' ');
                    for (var i = 0; i < classes.length; i++) {
                        e.classList.remove(classes[i]);
                    }
                }
                return;
            }
            if (e.setAttribute) {
                var classes = className.split(' ');
                for (var i = 0; i < classes.length; i++) {
                    var cls = classes[i];
                    if (hasClass(e, cls)) {
                        var rx = new RegExp('((\\s|^)' + cls + '(\\s|$))', 'g'),
                            cn = e.getAttribute('class');
                        cn = cn.replace(rx, ' ').replace(/ +/g, ' ').trim();
                        if (cn) {
                            e.setAttribute('class', cn);
                        } else {
                            e.removeAttribute('class');
                        }
                    }
                }
            }
        }
    }
    typedoc.removeClass = removeClass;

    function hasClass(e, className) {
        if (e && className) {
            if (e instanceof HTMLElement && e.classList && !isIE9()) {
                return e.classList.contains(className);
            }
            if (e.getAttribute) {
                var rx = new RegExp('(\\s|^)' + className + '(\\s|$)');
                return e && rx.test(e.getAttribute('class'));
            }
        }
        return false;
    }
    typedoc.hasClass = hasClass;

    function toggleClass(e, className, addOrRemove) {
        if (addOrRemove == undefined || addOrRemove == null) {
            addOrRemove = !hasClass(e, className);
        }
        if (addOrRemove) {
            addClass(e, className);
        } else {
            removeClass(e, className);
        }
    }
    typedoc.toggleClass = toggleClass;

    function closest(e, selector) {
        var matches = e ? (e.matches || e.webkitMatchesSelector || e.mozMatchesSelector || e.msMatchesSelector) : null;
        if (matches) {
            for (; e; e = e.parentNode) {
                if (e instanceof Element && matches.call(e, selector)) {
                    return e;
                }
            }
        }
        return null;
    }
    typedoc.closest = closest;
})(typedoc || (typedoc = {}));
var typedoc;
(function (typedoc) {
    var btn = document.querySelector('#btnScrollTop');
    if (btn) {
        if (window.scrollY >= 100) {
            $(btn).fadeIn(150);
        }
        window.addEventListener('scroll', function () {
            if (window.scrollY >= 100) {
                $(btn).fadeIn(150);
            } else {
                $(btn).fadeOut(150);
            }
        });
        btn.addEventListener('click', function () {
            $('body,html').animate({
                scrollTop: 0
            }, 100);
        });
    }
})(typedoc || (typedoc = {}));
var typedoc;
(function (typedoc) {
    var TreeView = (function () {
        function TreeView(el) {
            this.el = el;
            this.initTreeView();
        }
        TreeView.prototype.initTreeView = function () {
            var el = this.el;
            el.addEventListener('click', this._onClickHandler.bind(this), true);
            el.addEventListener('click', this.onClickHandler.bind(this), true);
            if (!el.querySelector('.node-expanded')) {
                var nodeWithChilds = el.querySelector('.has-child-nodes');
                if (nodeWithChilds) {
                    this.toggleNodeState(nodeWithChilds, true);
                }
            }
        };
        TreeView.prototype.applyPaddings = function (el) {
            var base = 4,
                step = 16,
                leaf = 18;
            var items = el.querySelectorAll('.gc-tree-item');
            Array.prototype.forEach.call(items, function (item) {
                var isLeaf = true;
                if (item.querySelector('[data-haschildren="true"]')) {
                    isLeaf = false;
                }
                var count = 0,
                    temp = item;
                while (temp && temp != el) {
                    if (temp.tagName.toLocaleLowerCase() == 'li') {
                        count++;
                    }
                    temp = temp.parentNode;
                }
                var pad = base + step * count;
                if (isLeaf) {
                    pad += leaf;
                }
                item.style.paddingLeft = pad + 'px';
            });
        };
        TreeView.prototype._onClickHandler = function (e) {
            if (e.target.tagName != 'A' && !(typedoc.hasClass(e.target, 'tree-item_icon') || typedoc.hasClass(e.target, 'tree-item_icon_img'))) {
                var parent_1 = typedoc.closest(e.target, '.gc-tree-item');
                if (parent_1) {
                    var anchor = parent_1.querySelector('a');
                    if (anchor) {
                        var evt = new MouseEvent('click', {
                            ctrlKey: e.ctrlKey,
                            cancelable: true,
                            bubbles: true
                        });
                        anchor.dispatchEvent(evt);
                        e.preventDefault();
                        e.stopImmediatePropagation();
                    }
                }
            }
        };
        TreeView.prototype.onClickHandler = function (e) {
            if (e.ctrlKey) {
                return;
            }
            var node = typedoc.closest(e.target, '.gc-tree-item');
            var selectedNode = document.querySelectorAll('.gc-tree-item[data-selected=true]');
            if (selectedNode) {
                for (let index = 0; index < selectedNode.length; index++) {
                    const element = selectedNode[index];
                    element.removeAttribute('data-selected');
                }
            }
            node.setAttribute('data-selected', 'true');
            var icon = node.querySelector('.tree-item_icon_img');
            if (!icon) {
                return;
            }
            var isExpanded = icon.getAttribute('data-expand') == 'true';
            if (node && typedoc.hasClass(e.target, 'tree_item_tag_container')) {
                var anchor = typedoc.closest(e.target, 'a');
                if (anchor && (anchor.href == window.location.href || anchor.href == window.location.href + '#')) {
                    this.toggleNodeState(node, !isExpanded);
                    e.preventDefault();
                }
            } else if (node && e.target.tagName != 'A') {
                this.toggleNodeState(node, !isExpanded);
            } else if (node && e.target.href == window.location.href) {
                this.toggleNodeState(node, !isExpanded);
                e.preventDefault();
            }
        };
        TreeView.prototype.toggleNodeState = function (node, state) {
            var icon = node.querySelector('.tree-item_icon_img');
            if (icon) {
                icon.setAttribute('data-expand', state);
            }
            var childs = node.nextElementSibling;
            if (childs && childs.tagName.toLocaleLowerCase() == 'ul') {
                childs.setAttribute('data-expand', state);
            }
        };
        TreeView.prototype._toggleNodeState = function (node, state) {
            typedoc.toggleClass(node, 'node-expanded', state);
            typedoc.toggleClass(node, 'node-collapsed', !state);
        };
        TreeView.prototype.setTreeVisibleState = function (state) {
            var sider = typedoc.closest(this.el, '.app-sider');
            if (sider) {
                sider.setAttribute('data-ishide', '' + !state);
                try {
                    Stickyfill.refreshAll();
                } catch (e) {}
            }
        };
        TreeView.prototype.isTreeVisisble = function () {
            var sider = typedoc.closest(this.el, '.app-sider');
            return sider && sider.getAttribute('data-ishide') == 'false';
        };
        TreeView.prototype.toggleTreeVisibility = function () {
            this.setTreeVisibleState(!this.isTreeVisisble());
        };
        TreeView.prototype.setTreeVisibleStateMobile = function (state) {
            var sider = typedoc.closest(this.el, '.app-sider');
            if (sider) {
                sider.setAttribute('data-ishide-mobile', '' + !state);
                document.body.style.overflow = state ? 'hidden' : 'auto';
            }
        };
        TreeView.prototype.isTreeVisisbleMobile = function () {
            var sider = typedoc.closest(this.el, '.app-sider');
            return sider && sider.getAttribute('data-ishide-mobile') != 'true';
        };
        TreeView.prototype.toggleTreeVisibilityMobile = function () {
            this.setTreeVisibleStateMobile(!this.isTreeVisisbleMobile());
        };
        return TreeView;
    }());
    var el = document.querySelector('#treeNav'),
        tv;
    if (el) {
        tv = new TreeView(el);
    }
    var navHider = document.querySelector('.menu-icon-container');
    if (navHider && tv) {
        navHider.addEventListener('click', function (e) {
            tv.toggleTreeVisibility();
            navHider.setAttribute('expand-state', tv.isTreeVisisble() ? 'collapse' : 'expand');
        });
    }
    var mobNavHider = document.querySelector('.mobile_tree-menu');
    if (mobNavHider && tv) {
        mobNavHider.addEventListener('click', function () {
            tv.toggleTreeVisibilityMobile();
            var icon = mobNavHider.querySelector('.mobile_tree-menu-icon');
            if (icon) {
                icon.setAttribute('data-isshow', tv.isTreeVisisbleMobile());
            }
        });
    }
    var mobHeaderNavMobile = document.querySelector('#headerNavMobile'),
        toggleIcon = document.querySelector('.app-hanburger');
    if (toggleIcon && mobHeaderNavMobile) {
        toggleIcon.addEventListener('click', function () {
            var isVisible = mobHeaderNavMobile.getAttribute('data-ishide') !== 'true';
            var appHeader = typedoc.closest(toggleIcon, '.app-header');
            if (isVisible) {
                mobHeaderNavMobile.setAttribute('data-ishide', 'true');
                if (appHeader) {
                    appHeader.removeAttribute('data-menu-show');
                }
            } else {
                mobHeaderNavMobile.removeAttribute('data-ishide');
                if (appHeader) {
                    appHeader.setAttribute('data-menu-show', 'true');
                }
            }
            document.body.style.overflow = isVisible ? 'auto' : 'hidden';
        });
    }
})(typedoc || (typedoc = {}));