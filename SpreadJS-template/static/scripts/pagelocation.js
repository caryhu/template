'use strict';

$(document).ready(function () {
  var currentSectionNav, target;
  openHashTOC();
  // If an anchor hash is in the URL highlight the menu item
  highlightActiveHash();
  // If a specific page section is in the URL highlight the menu item
  highlightActiveSection();
  // If a specific page section is in the URL scroll that section up to the top
  currentSectionNav = $('#' + getCurrentSectionName() + '-nav');
  if (currentSectionNav.position()) {
    $('nav').scrollTop(currentSectionNav.position().top - 170);
  }
  // function to scroll to anchor when clicking an anchor link
  $('a[href*="#"]:not([href="#"])').click(function () {
    if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
      if(this.hash.indexOf('.') > -1){
        target = $(`[id*="${this.hash.replace('#','')}"]`);
      } else {
        target = $(this.hash);
      }
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top - 170
        }, 247);
      }
    }
  });
});

// If a new anchor section is selected, change the hightlighted menu item
$(window).bind('hashchange', function (event) {
  highlightActiveHash(event);
});

function highlightActiveHash(event) {
  var oldUrl, oldSubSectionElement;

  // check for and remove old hash active state
  if (event && event.originalEvent.oldURL) {
    oldUrl = event.originalEvent.oldURL;

    if (oldUrl.indexOf('#') > -1) {
      oldSubSectionElement = $('#' + getCurrentSectionName() + '-' + oldUrl.substring(oldUrl.indexOf('#') + 1) + '-nav');

      if (oldSubSectionElement) {
        oldSubSectionElement.removeClass('active');
      }
    }
  }

  if (getCurrentHashName()) {
    $('#' + getCurrentSectionName() + '-' + getCurrentHashName() + '-nav').addClass('active');
  }
}

function highlightActiveSection() {
  var pageId = getCurrentSectionName();

  $('#' + pageId + '-nav').addClass('active');
}

function getCurrentSectionName() {
  var path = window.location.pathname;
  var pageUrl = path.split('/').pop();

  var sectionName = pageUrl.substring(0, pageUrl.indexOf('.'));

  // remove the wodr module- if its in the url
  sectionName = sectionName.replace('module-', '');

  return sectionName;
}

function getCurrentHashName() {
  var pageSubSectionId;
  var pageSubSectionHash = window.location.hash;

  if (pageSubSectionHash) {
    pageSubSectionId = pageSubSectionHash.substring(1);

    return pageSubSectionId;
  }

  return false;
}

function openHashTOC() {
  var currentHash = getCurrentHashName();
  if(currentHash) {
    var pathArray = typedoc.$window[0].location.pathname.toString().split('/');
    pathArray = pathArray[pathArray.length - 1].split('.html')[0]
    if(currentHash.indexOf('.') > -1) {
      currentHash = pathArray + currentHash;
      console.log(currentHash)
    } else {
      currentHash = pathArray + '#' + currentHash;
    }
    var node = $(`[data-name*="${currentHash}"]`)[0]
    node.dataset.selected = 'true'
    node.parentNode.parentNode.dataset.expand = 'true'
    node.parentNode.parentNode.previousSibling.childNodes[1].childNodes[1].dataset.expand = 'true'
  } else {
    var pathArray = typedoc.$window[0].location.pathname.toString().split('/');
    pathArray = pathArray[pathArray.length - 1].split('.html')[0].split('.');
    console.log(pathArray)
    var path = pathArray[pathArray.length - 1]
    if(path === 'index') {
      var node = document.getElementById('SpreadJS-nav')
      node.dataset.selected = 'true'
      return
    }
    var node = document.getElementById(path + '-nav')
    node.dataset.selected = 'true'
  }
}
